// Remove the transition class

const scaleFunction = () => {
const square = document.querySelector('#ultraVideo1');
square.classList.remove('square-animation');

// Create the observer, same as before:
const observer = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square.classList.remove('scaleStart');
        setTimeout(() => {
            square.classList.add('square-animation');
        }, 0.5)
     
      return;
    } else{
      square.classList.add('scaleStart');
    }

    square.classList.remove('square-animation');
  });
});

observer.observe(document.querySelector('#ultraVideo1'));





// Remove the transition class
const square2 = document.querySelector('#ultraVideo2');
square2.classList.remove('square-animation');

// Create the observer, same as before:
const observer2 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square2.classList.remove('scaleStart');
      square2.classList.add('square-animation');
      return;
    } else{
      square2.classList.add('scaleStart');
    }

    square2.classList.remove('square-animation');
  });
});

observer2.observe(document.querySelector('#ultraVideo2'));





const square3 = document.querySelector('#ultraVideo3');
square3.classList.remove('square-animation');

// Create the observer, same as before:
const observer3 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square3.classList.remove('scaleStart');
        setTimeout(() => {
            square3.classList.add('square-animation');
        }, 0.5)
     
      return;
    } else{
      square3.classList.add('scaleStart');
    }

    square3.classList.remove('square-animation');
  });
});

observer3.observe(document.querySelector('#ultraVideo3'));


}

export default scaleFunction;
